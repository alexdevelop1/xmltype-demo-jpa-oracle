package io.switchbit.dao.types.order;

import io.switchbit.service.xml.order.Order;

public class OrderUserType extends CustomUserType<Order> {

    public final static String NAME = "io.switchbit.dao.types.order.OrderUserType";

    public OrderUserType(Class<Order> clazz) {
        super(clazz);
    }

    public OrderUserType() {
        super(Order.class);
    }

}
