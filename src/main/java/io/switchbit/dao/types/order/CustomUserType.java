package io.switchbit.dao.types.order;

import oracle.jdbc.driver.OracleConnection;
import oracle.xdb.XMLType;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.usertype.UserType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.Serializable;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static io.switchbit.dao.types.utils.XmlUtils.removeNamespacesXSLT;
import static java.nio.charset.StandardCharsets.UTF_8;
import static javax.xml.bind.Marshaller.JAXB_ENCODING;

public class CustomUserType<T> implements UserType {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomUserType.class);


    private JAXBContext jaxbContext;

    private Class<T> clazz;

    public CustomUserType(Class<T> clazz) {

        try {
            this.jaxbContext = JAXBContext.newInstance(clazz);
        } catch (JAXBException e) {

            LOGGER.error("Cannot initialize JAXBContext", e);

            throw new RuntimeException("Cannot initialize JAXBContext", e);
        }

        this.clazz = clazz;
    }

    /**
     * Здесь мы зарегистрировали числовой код, SQL-типа данных из oracle.xdb.XMLType
     * (библиотека oracle.xdb)
     * public static final int _SQL_TYPECODE = 2007;
     *
     * @return возвращает массив, зарегистрированных типов данных
     */
    @Override
    public int[] sqlTypes() {

        return new int[]{
            XMLType._SQL_TYPECODE};
    }

    @SuppressWarnings("rawtypes")
    @Override
    public Class returnedClass() {
        return clazz;
    }

    /**
     * Сравниваются 2 экземпляра класса сопоставленного с указанным типом
     * для сохранения "Равенства"( Equality).
     * Равенство состояния постоянства (persistent state).
     * Проверяет на равенства два значения .
     * Мы считаем наш массив неизменным (хоть в Java и можно менять содержимое массива,
     * но в этом случае мы это административно запрещаем;
     * поэтому просто проверяем на равенство ссылки.
     */
    @Override
    public boolean equals(Object x, Object y) throws HibernateException {
        return (x != null) && x.equals(y);
    }

    /**
     * Получаем hashcode для экземляра, совместимый с сохранением равенства
     * (persistence "equality")
     * Перевызываем метод hashCode() целевого объекта (когда мы создаем пользовательские
     * типы, всегда переопределяется метод hashCode())
     */
    @Override
    public int hashCode(Object x) throws HibernateException {
        return (x != null) ? x.hashCode() : 0;
    }


    /**
     * Получение значения из поля типа XmlType.
     *
     * @param rs    результирующий набор, после выполнения запроса
     * @param names имена полей, в данном запросе
     * @return возвращает полученное значение в сопоставленном объекте типа XMLType
     */
    private XMLType getXmlTypeFromResultSet(ResultSet rs, String[] names) throws SQLException {

        XMLType xmlType;

        try {
            xmlType = (XMLType) rs.getObject(names[0]);

        } catch (SQLException e) {

            LOGGER.error("Cannot get value from field of type XmlType", e);

            throw new SQLException("Cannot get value from field of type XmlType", e);
        }

        return xmlType;
    }

    /**
     * Значение этого метода, используется объектом JAXB для демаршаллинга из класса org.w3c.dom.Document,
     * возвращающего oracle.xdb.XMLType для нашего класса, который мы расширяем от данного универсального
     * класса
     * Полученное значение (которое содержит Xml-документ) переводится в объект типа
     * org.w3c.dom.Document, чтобы затем можно было выполнить  unmarshall
     * Document treeXml -*Это дерево XML-документа, созданное из полученного
     * значение, которое мы достали из столбца с типом данных XmlType
     *
     * @param xmlType полученный xml-документ в оболочки типа от oracle -  XMLType
     * @return возвращает объект типа Document
     */
    private Document getXmlDocumentTree(XMLType xmlType) throws SQLException {

        Document treeXml = null;

        if (xmlType != null) {

            try {
                treeXml = xmlType.getDocument();
            } catch (SQLException e) {

                LOGGER.error("Cannot create a tree of org.w3c.dom.Document for Xml-document", e);

                throw new SQLException("Cannot create a tree of org.w3c.dom.Document for Xml-document", e);
            }
        }

        return treeXml;
    }

    /**
     * В объект Object(это суперкласс, для любого типа данных, который будет расширять данный класс)
     * сохраняем структуру XML-документа, после демаршаллинга
     * Метод выполняет демаршаллинг и передает всю структуру xml-документа, в объект, который соответсвует
     * струтуре тегов , разбираемого xml-документа
     *
     */
    private Object performUnmarshalling(Document treeXml) throws SQLException {

        Object objectFromXml = null;

        final Unmarshaller unmarshaller;

        try {
            unmarshaller = jaxbContext.createUnmarshaller();
        } catch (JAXBException e) {

            LOGGER.error("Cannot create Unmarshaller", e);

            throw new SQLException("Cannot create Unmarshaller", e);
        }

        try {
            objectFromXml = unmarshaller.unmarshal(treeXml, clazz).getValue();

        } catch (JAXBException e) {
            e.printStackTrace();
        }

        return objectFromXml;
    }

    /**
     * getObject(names[0]) принимает название столбца, согласно которому из результирующего
     * набора, получаем значение и преобразовываем полученное значение в тип XMLType (тип данных определенный в Oracle)
     *
     * @param rs      результирующий набор из базы данных, после чтения
     * @param names   названия полей, для резульитрующего набора определенной таблицы базы данных
     * @param session Определяет внутренний контракт, совместно используемый
     *                между  org.hibernate.Session иorg.hibernate.StatelessSession,
     *                так же используемый другими частями Hibernate реализаторов (таким как
     *                org.hibernate.type.Type,  EntityPersister и
     *                org.hibernate.persister.collection.CollectionPersister}
     * @param owner   текущий получаемый из базы экземпляр сущности.
     *                <p>
     *                names[0] - это говорит о том, что мы получаем первую строку из массива,
     *                которая и будет для нас названием столбца, в котором находится наш тип данных.
     */
    @Override
    public Object nullSafeGet(ResultSet rs,
                              String[] names,
                              SharedSessionContractImplementor session,
                              Object owner) throws HibernateException, SQLException {

        XMLType xmlType = getXmlTypeFromResultSet(rs, names);

        Document treeXml = getXmlDocumentTree(xmlType);

        Object objectFromXml = performUnmarshalling(treeXml);

        return objectFromXml;
    }


    private Marshaller createMarshaller() {

        Marshaller marshaller = null;

        try {
            marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(JAXB_ENCODING, UTF_8.name());

        } catch (JAXBException e) {

            LOGGER.error("Cannot create Marshaller", e);
            e.printStackTrace();
        }

        return marshaller;
    }

    /**
     * сериализация xml-документа из объекта в сторку
     *
     * @param value объект, который будет преобразован в строку, которая будет затем
     *              сериализована в строку, содержащую xml-документ,
     *              Данный xml-документ будет выполенен в  соответсвующей структуре, которая
     *              будет посторена из полей и значений предоставленного объекта
     * @return возвращает строку, в которой будет записан XML-документ
     */
    private String jaxbToString(final Object value) {

        StringWriter stringWriter = new StringWriter();

        Marshaller marshaller = createMarshaller();

        try {
            marshaller.marshal(value, stringWriter);
        } catch (JAXBException e) {
            LOGGER.error("Cannot perform marshalling", e);
            e.printStackTrace();
        }

        return stringWriter.toString();
    }


    private OracleConnection getOracleConnection(PreparedStatement st) {

        OracleConnection unwrapConnection = null;
        try {
            Connection connection = st.getConnection();
            unwrapConnection = connection.unwrap(OracleConnection.class);
        } catch (SQLException e) {
            LOGGER.error("Cannot unwrap a connection from Oracle", e);
            e.printStackTrace();
        }

        return unwrapConnection;
    }


    private XMLType getXmlTypeForSave(OracleConnection unwrapConnection, String xmlString) {

        XMLType xmlType = null;

        try {
            xmlType = new XMLType(unwrapConnection, xmlString);
        } catch (SQLException e) {
            LOGGER.error("Cannot prepare XMLType for saving", e);
            e.printStackTrace();
        }

        return xmlType;
    }


    private void runSavingToBase(PreparedStatement st,
                                 int index, XMLType xmlType) {

        try {
            st.setObject(index, xmlType);
        } catch (SQLException e) {

            LOGGER.error("Cannot perform a record to Database", e);

            e.printStackTrace();
        }
    }

    /**
     * Служит для записи содержимого объекта внутрь БД.  Преобразовывает значения указанного типа объекта в вид,
     * пригодный для записи в базу
     * В nullSafeSet() передаётся JDBC PreparedStatement, аргументы которого заполняются
     * для NativeJdbcExtractor extractor, необходима библиотека из
     * <groupId>org.springframework</groupId>
     * <artifactId>spring-jdbc</artifactId>
     * <version>4.3.27.RELEASE</version>,
     * версии начиная с 5-й, убрали данный класс.
     * <p>
     * st.setObject(index, xmlType);        Здесь мы задаем значение в указанный параметр (index)
     * index - пордяковый номер передаваемого значения, в запросе PreparedStatement
     * Здесь важно по прежнему устанавливат объект, то есть выполнять данный метод,
     * даже если значение xmlType = null. Если этого не сделать, тогда можем получить
     * исключение "org.h2.jdbc.JdbcSQLException: Parameter "#?" is not set; SQL statement",
     * которое сообщает, что подготовленный запрос (PreparedStatement), не получил установленный
     * параметр
     *
     * @param st    аргументы данного объекта автоматически заполняются. Этот объект содержит подготовленное
     *              sql-выражение
     * @param value тип объекта, из полей которого будут доставаться значения и согласно структуре данного объекта, будет
     * @param index порядковый номер параметра, в запросе PreparedStatement
     */
    @Override
    public void nullSafeSet(PreparedStatement st,
                            Object value, int index,
                            SharedSessionContractImplementor session) throws HibernateException {

        XMLType xmlType = null;
        if (value != null) {

            OracleConnection unwrapConnection = getOracleConnection(st);



            String xmlString = jaxbToString(value);

            String xmlWithoutNamespacesXSLT = removeNamespacesXSLT(xmlString);

            xmlType = getXmlTypeForSave(unwrapConnection, xmlWithoutNamespacesXSLT);

            runSavingToBase(st, index, xmlType);
        }

    }





    /**
     * Возвращает глубокую копию состояния сохранения (persistent state), останавливаясь на
     * сущностях и коллекциях. Это не является необходимым для копирования
     * неизменямых объектов, или null значений, в этом случае, можно просто безопасно вернуть аргумент
     *
     * @param value значение объекта для клонирования, может быть null
     */
    @Override
    public Object deepCopy(Object value) throws HibernateException {
        return value;
    }

    /**
     * Указываем на то, изменяемые ли объекты с которыми работаем или нет
     */
    @Override
    public boolean isMutable() {
        return false;
    }

    /**
     * Преобразуем объект в его кэшируемое представление. По крайней мере, этот метод, должен выполнять
     * глубокое копирование, если тип является изменяемым (мы тогда должны были определить в методе
     * isMutable() , что возвращается true. Однако этого может быть недостаточно для некоторых реализаций, например,
     * ассоциации для данного объекта (поля которые включаеют в себя связи с другими объектами) должны быть кэшированы
     * как значения идентификаторов (дополнительная операция)
     *
     * @param value объект для кэширования
     * @return возрвращает кэшируемое представление объекта
     */
    @Override
    public Serializable disassemble(Object value) throws HibernateException {
        return (Serializable) value;
    }

    /**
     * Реконструирует объект из кэшируемого объекта. По крайней мере этот метод должен выполнять
     * глубокое копирование если тип является изменяемым (дополняительная работа)
     *
     * @param cached - объект для кэширования
     * @param owner            владелец кэшируемого объекта (то есть объект, который кэшируется)
     * @return возвращает реконтструированный объект пригодный для кэширования
     */
    @Override
    public Object assemble(Serializable cached, Object owner) throws HibernateException {
        return cached;
    }

    /**
     * Во время слияния, замените существующее (целевое - o1) значение в сущности,
     * с которой мы объединяемся с новым (исходным) значением от отсоединенной сущности ( detached entity),
     * которую мы объединяем.
     * Для неизменяемых объектов (isMutable() , - возвращается true) или значений null, можно просто
     * вернуть первый параметр. Для объектов с значениями компонентов, может имеет смысл рекурсивно заменять
     * значения значений компонентов.
     *
     * @param original  значение из отсоединенной сущности ( detached entity),
     *           которая подлежит объединению (being merged)
     * @param target значение в управляемой сущности (managed entity)
     * @return возвращает значение для слияния (to be merged)
     */
    @Override
    public Object replace(Object original, Object target, Object owner) throws HibernateException {
        return original;
    }
}
