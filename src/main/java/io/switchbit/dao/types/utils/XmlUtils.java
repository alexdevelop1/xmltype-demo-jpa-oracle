package io.switchbit.dao.types.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamSource;
import java.io.StringReader;
import java.io.StringWriter;

public class XmlUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(XmlUtils.class);

    public static String removeNamespacesXSLT(String xml) { // good work

        try {
            StringReader reader = new StringReader(xml);
            StringWriter writer = new StringWriter();

            runTransformXml(reader, writer);

            String result = writer.toString();
            return result;

        } catch (Exception e) {
            LOGGER.error("removeNamespacesXSLT : deleting namespaces failed: "+ e.getMessage());
        }
        return "";
    }


    private static void runTransformXml(StringReader reader, StringWriter writer){

        Transformer transformer = createTransformer();

        try {
            transformer.transform(
                new StreamSource(reader),
                new javax.xml.transform.stream.StreamResult(writer));
        } catch (TransformerException e) {
            e.printStackTrace();
        }

    }

    private static Transformer createTransformer(){
        TransformerFactory factory = TransformerFactory.newInstance();
        Transformer transformer = null;  // TODO only for demo purposes, cache transformer into static library to prevent performance issue
        try {
            transformer = factory.newTransformer(

                new StreamSource("classpath:no-namespaces.xslt"));

        } catch (TransformerConfigurationException e) {

            LOGGER.error("Cannot create Transformer : "+ e.getMessage());
        }
         return transformer;
    }

}
