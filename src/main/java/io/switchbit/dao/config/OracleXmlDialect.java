package io.switchbit.dao.config;

import oracle.xdb.XMLType;
import org.hibernate.boot.model.TypeContributions;
import org.hibernate.dialect.Oracle10gDialect;
import org.hibernate.service.ServiceRegistry;

public class OracleXmlDialect extends Oracle10gDialect {

    public static final String XMLTYPE = "XMLTYPE";

    public OracleXmlDialect() {
        super();
        registerColumnType(XMLType._SQL_TYPECODE, XMLTYPE);
        registerHibernateType(XMLType._SQL_TYPECODE, XMLTYPE);
    }


    @Override
    public void contributeTypes(TypeContributions typeContributions, ServiceRegistry serviceRegistry) {

        super.contributeTypes(typeContributions, serviceRegistry);
    }

}
