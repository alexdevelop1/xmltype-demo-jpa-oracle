package io.switchbit.dao.repository.order;

import io.switchbit.dao.domain.order.OrderEntityXml;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface OrderEntityXmlRepository extends CrudRepository<OrderEntityXml, Long> {


    @Query(value = "select * from T_ORDER_XML where XMLEXISTS('//orderItem[sku = \"?1\"]' passing ORDER_XML)",
        nativeQuery = true)
    Iterable<OrderEntityXml> findByOrderXmlWithSku(String sku);
}
