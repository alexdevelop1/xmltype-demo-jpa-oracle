package io.switchbit.dao.domain.order;

import io.switchbit.service.xml.order.Order;
import io.switchbit.dao.types.order.OrderUserType;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;
import java.util.Objects;


@Entity
@Table(name = "t_order_xml")
public class OrderEntityXml {

    @Id
    @SequenceGenerator(name = "jpa.Sequence.t.order", sequenceName = "T_ORDER_SEQ", allocationSize = 1)
    @GeneratedValue(generator = "jpa.Sequence.t.order", strategy = GenerationType.SEQUENCE)
    private Long id;

    private String customer;

    @Type(type = OrderUserType.NAME)
    @Column(name = "order_xml")
    private Order order;

    public OrderEntityXml() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderEntityXml that = (OrderEntityXml) o;
        return Objects.equals(id, that.id) &&
            Objects.equals(customer, that.customer) &&
            Objects.equals(order, that.order);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, customer, order);
    }

    @Override
    public String toString() {
        return "OrderEntityXml{" +
            "id=" + id +
            ", customer='" + customer + '\'' +
            ", order=" + order +
            '}';
    }
}
