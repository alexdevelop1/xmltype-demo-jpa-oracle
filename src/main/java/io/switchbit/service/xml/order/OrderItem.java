package io.switchbit.service.xml.order;

import java.util.Objects;

public class OrderItem {

    private String sku;

    private Double price;

    public OrderItem() {
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getSku() {
        return sku;
    }

    public Double getPrice() {
        return price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderItem orderItem = (OrderItem) o;
        return Objects.equals(sku, orderItem.sku) &&
            Objects.equals(price, orderItem.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sku, price);
    }

    @Override
    public String toString() {
        return "OrderItem{" +
            "sku='" + sku + '\'' +
            ", price=" + price +
            '}';
    }
}
