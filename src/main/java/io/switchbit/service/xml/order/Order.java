package io.switchbit.service.xml.order;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@XmlRootElement
@JacksonXmlRootElement(localName = "order")
public class Order implements Serializable {


    private String customer;

    @XmlElement(name = "orderItem")
    @JacksonXmlProperty(localName = "orderItem")
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<OrderItem> orderItems = new ArrayList<>();

    public Order() {
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public List<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return Objects.equals(customer, order.customer) &&
            Objects.equals(orderItems, order.orderItems);
    }

    @Override
    public int hashCode() {
        return Objects.hash(customer, orderItems);
    }

    @Override
    public String toString() {
        return "Order{" +
            "customer='" + customer + '\'' +
            ", orderItems=" + orderItems +
            '}';
    }
}
