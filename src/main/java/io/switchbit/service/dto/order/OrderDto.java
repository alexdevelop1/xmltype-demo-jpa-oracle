package io.switchbit.service.dto.order;

import io.switchbit.service.xml.order.Order;

import java.util.Objects;

public class OrderDto {

    private Long id;

    private String customer;

    private Order order;

    public OrderDto() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderDto orderDto = (OrderDto) o;
        return Objects.equals(id, orderDto.id) &&
            Objects.equals(customer, orderDto.customer) &&
            Objects.equals(order, orderDto.order);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, customer, order);
    }

    @Override
    public String toString() {
        return "OrderDto{" +
            "id=" + id +
            ", customer='" + customer + '\'' +
            ", order=" + order +
            '}';
    }
}
