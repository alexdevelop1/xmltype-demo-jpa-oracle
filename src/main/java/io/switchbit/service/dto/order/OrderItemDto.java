package io.switchbit.service.dto.order;

import java.util.Objects;

public class OrderItemDto {

    private String sku;

    private Double price;

    public OrderItemDto(String sku, Double price) {
        this.sku = sku;
        this.price = price;
    }

    public OrderItemDto() {
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderItemDto that = (OrderItemDto) o;
        return Objects.equals(sku, that.sku) &&
            Objects.equals(price, that.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sku, price);
    }

    @Override
    public String toString() {
        return "OrderItemDto{" +
            "sku='" + sku + '\'' +
            ", price=" + price +
            '}';
    }
}
