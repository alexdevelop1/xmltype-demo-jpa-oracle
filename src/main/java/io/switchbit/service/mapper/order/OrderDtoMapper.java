package io.switchbit.service.mapper.order;

import io.switchbit.dao.domain.order.OrderEntityXml;
import io.switchbit.service.mapper.CommonMapper;
import io.switchbit.service.mapper.CycleAvoidingMappingContext;
import io.switchbit.service.xml.order.Order;
import org.mapstruct.Mapper;

import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static io.switchbit.service.mapper.order.MapperUtils.convertToDto;
import static io.switchbit.service.mapper.order.MapperUtils.convertToEntity;

@Mapper(componentModel = "spring")
public interface OrderDtoMapper extends CommonMapper<Order, OrderEntityXml> {

    @Override
    default Order toDto(OrderEntityXml orderEntityXml, CycleAvoidingMappingContext context) {

        return convertToDto(orderEntityXml);
    }

    @Override
    default OrderEntityXml toEntity(Order order, CycleAvoidingMappingContext context) {

        return convertToEntity(order);
    }


    @Override
    default Iterable<Order> toListDto(Iterable<OrderEntityXml> entityList) {

        Iterable<Order> collect = StreamSupport.stream(entityList.spliterator(), false)
            .map(MapperUtils::convertToDto)
            .collect(Collectors.toList());

        return collect;
    }
}
