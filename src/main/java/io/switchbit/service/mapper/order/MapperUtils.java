package io.switchbit.service.mapper.order;

import io.switchbit.dao.domain.order.OrderEntityXml;
import io.switchbit.service.xml.order.Order;
import io.switchbit.service.xml.order.OrderItem;

import java.util.List;

public class MapperUtils {

    public static Order convertToDto(OrderEntityXml orderEntityXml){
        Order order = new Order();

        String customer = orderEntityXml.getCustomer();
        order.setCustomer(customer);

        Order xmlOrder = orderEntityXml.getOrder();
        List<OrderItem> orderItems = xmlOrder.getOrderItems();
        order.setOrderItems(orderItems);

        return order;
    }

    public static OrderEntityXml convertToEntity(Order order){

        OrderEntityXml orderEntityXml = new OrderEntityXml();
        String customer = order.getCustomer();
        orderEntityXml.setCustomer(customer);

        List<OrderItem> orderItems = order.getOrderItems();

        Order orderInEntity = new Order();

        orderInEntity.setOrderItems(orderItems);
        orderInEntity.setCustomer(customer);

        orderEntityXml.setOrder(orderInEntity);

        return orderEntityXml;
    }
}
