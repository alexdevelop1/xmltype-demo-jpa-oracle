package io.switchbit.service.read;

import io.switchbit.service.xml.order.Order;

public interface OrderReadService {

  Iterable <Order> getListOrder();

    Iterable <Order> getByOrderXmlWithSku(String sku);
}
