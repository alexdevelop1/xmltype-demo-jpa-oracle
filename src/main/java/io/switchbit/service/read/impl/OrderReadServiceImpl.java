package io.switchbit.service.read.impl;

import io.switchbit.dao.domain.order.OrderEntityXml;
import io.switchbit.dao.repository.order.OrderEntityXmlRepository;
import io.switchbit.service.mapper.CycleAvoidingMappingContext;
import io.switchbit.service.mapper.order.OrderDtoMapper;
import io.switchbit.service.read.OrderReadService;
import io.switchbit.service.xml.order.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderReadServiceImpl implements OrderReadService {

    private OrderEntityXmlRepository repository;

    private OrderDtoMapper mapper;

    private CycleAvoidingMappingContext context;

    @Autowired
    public OrderReadServiceImpl(OrderEntityXmlRepository repository,
                                OrderDtoMapper mapper,
                                CycleAvoidingMappingContext context) {
        this.repository = repository;
        this.mapper = mapper;
        this.context = context;
    }

    @Override
    public Iterable<Order> getListOrder() {

        Iterable<OrderEntityXml> orderEntities = this.repository.findAll();

        Iterable<Order> orders = toListDto(orderEntities);

        return orders;
    }


    @Override
    public Iterable<Order> getByOrderXmlWithSku(String sku) {

        Iterable<OrderEntityXml> xmlWithSku = this.repository.findByOrderXmlWithSku(sku);

        Iterable<Order> orders = toListDto(xmlWithSku);

        return orders;
    }


    /**
     * transform objects of  entity type to objects of dto types
     *
     * @param entities
     * @return
     */
    private Iterable<Order> toListDto(Iterable<OrderEntityXml> entities) {
        return this.mapper.toListDto(entities);
    }

}
