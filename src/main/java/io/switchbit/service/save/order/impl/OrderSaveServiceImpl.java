package io.switchbit.service.save.order.impl;

import io.switchbit.dao.domain.order.OrderEntityXml;
import io.switchbit.dao.repository.order.OrderEntityXmlRepository;
import io.switchbit.service.mapper.CycleAvoidingMappingContext;
import io.switchbit.service.mapper.order.OrderDtoMapper;
import io.switchbit.service.save.order.OrderSaveService;
import io.switchbit.service.xml.order.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.StringWriter;

@Service
public class OrderSaveServiceImpl implements OrderSaveService {

    private OrderEntityXmlRepository repository;

    private OrderDtoMapper mapper;

    private CycleAvoidingMappingContext context;

    @Autowired
    public OrderSaveServiceImpl(OrderEntityXmlRepository repository,
                                OrderDtoMapper mapper, CycleAvoidingMappingContext context) {
        this.repository = repository;
        this.mapper = mapper;
        this.context = context;
    }

    @Transactional
    @Override
    public Order placeOrderToBaseXml(Order orderXml, StringWriter strOrderXml) {

        OrderEntityXml orderEntityXml = toEntity(orderXml);
        OrderEntityXml entitySaved = setOrder(orderEntityXml);

        Order order = toOrder(entitySaved);

        return order;
    }


    private OrderEntityXml setOrder(OrderEntityXml entity) {

        return this.repository.save(entity);
    }


    private Order toOrder(OrderEntityXml entity){

       return this.mapper.toDto(entity, context);
    }

    private OrderEntityXml toEntity(Order orderXml){

        OrderEntityXml orderEntityXml = this.mapper.toEntity(orderXml, context);

        return orderEntityXml;
    }
}
