package io.switchbit.service.save.order;

import io.switchbit.service.xml.order.Order;

import java.io.StringWriter;

public interface OrderSaveService {

    Order placeOrderToBaseXml (Order orderXml, StringWriter strOrderXml);
}
