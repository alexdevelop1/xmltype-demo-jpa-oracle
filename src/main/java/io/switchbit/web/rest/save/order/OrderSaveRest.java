package io.switchbit.web.rest.save.order;

import io.switchbit.service.save.order.OrderSaveService;
import io.switchbit.service.xml.order.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.xml.bind.JAXB;
import java.io.StringWriter;

@RestController
public class OrderSaveRest {

    private OrderSaveService service;

    @Autowired
    public OrderSaveRest(OrderSaveService service) {
        this.service = service;
    }

    @PostMapping(path = "orders", produces = MediaType.APPLICATION_XML_VALUE)
    public Order placeXml(@RequestBody Order order){

        StringWriter orderXml = new StringWriter();
        JAXB.marshal(order, orderXml);

        Order dto = this.service.placeOrderToBaseXml(order, orderXml);

        return dto;
    }
}
