package io.switchbit.web.rest.read;


import io.switchbit.service.read.OrderReadService;
import io.switchbit.service.xml.order.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OrderReadRest {

    private OrderReadService service;

    @Autowired
    public OrderReadRest(OrderReadService service) {
        this.service = service;
    }

    @GetMapping(path = "read", produces = MediaType.APPLICATION_XML_VALUE)
    public Iterable<Order> getList(){

        Iterable<Order> listOrder = this.service.getListOrder();

        return listOrder;
    }

    @GetMapping(path = "read/{sku}", produces = MediaType.APPLICATION_XML_VALUE)
    public Iterable<Order> getXmlWithSku(String sku){

        Iterable<Order> orders = this.service.getByOrderXmlWithSku(sku);

        return orders;
    }


}
